
#include "mem.h"

#include "tests.h"

int main() {
	void* heap = heap_init( 8192 );

	test1( heap );
	test2( heap );
	test3( heap );
	test4( heap );
	test5( heap );

	return 0;
} 
