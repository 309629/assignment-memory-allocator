
#define _DEFAULT_SOURCE

#include "tests.h"

#include <assert.h>
#include <stdbool.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

static struct block_header* get_header( void* addr ) {
	return (struct block_header*) ( ((uint8_t*) addr) - offsetof( struct block_header, contents) );
}

static struct block_header* block_after( struct block_header* block ) {
	return (struct block_header*) ( block->contents + block->capacity.bytes );
}

static void assert_next_and_continious( struct block_header* block1, struct block_header* block2 ) {
	assert( block1->next == block2 );
	assert( block_after( block1 ) == block2 );
}

static void dbg_heap( const char* msg, void* heap ) {
	fprintf( stderr, "\n%s\n", msg );
	debug_heap( stderr, heap );
}

void test1( void* heap_start ) {
	fprintf( stdout, "\nTEST: Allocating one block\n" );
	
	dbg_heap( "Heap condition before allocation:", heap_start );
	
	fprintf( stderr, "\nAllocating 100 bytes..\n" );
	void* addr = _malloc( 100 );
	
	assert( addr );
	
	dbg_heap( "Heap condition after allocation:", heap_start );
	
	struct block_header* heap_block = heap_start;
	
	assert( heap_block->capacity.bytes == 100 );
	assert( !heap_block->is_free );
	assert( heap_block->contents == addr );
	
	fprintf( stdout, "\nTest passed!\n" );
	
	_free( addr );
}

void test2( void* heap_start ) {
	fprintf( stdout, "\nTEST: Allocating two blocks and freeing one\n" );
	
	dbg_heap( "Heap condition before allocation:", heap_start );
	
	fprintf( stderr, "\nAllocating two blocks: 100 bytes and 200 bytes..\n" );
	
	void* addr1 = _malloc( 100 );
	void* addr2 = _malloc( 200 );
	
	dbg_heap( "Heap condition after allocation:", heap_start );
	
	assert( addr1 );
	assert( addr2 );
	
	struct block_header* block1 = heap_start;
	
	assert( block1->contents == addr1 );
	
	struct block_header* block2 = (struct block_header*) ( ( (uint8_t*) addr2 ) - offsetof( struct block_header, contents ));
	
	assert_next_and_continious( block1, block2 );
	
	fprintf( stderr, "\nFreeing first block..\n" );
	_free( addr1 );
	
	dbg_heap( "Heap condition after freeing:", heap_start );
	
	assert( block1->is_free );
	
	fprintf( stdout, "\nTest passed!\n" );
	
	_free( addr2 );
}

void test3( void* heap_start ) {
	fprintf( stdout, "\nTEST: Allocating five blocks and freeing two\n" );
	
	dbg_heap( "Heap condition before allocation:", heap_start );
	
	fprintf( stderr, "\nAllocating five blocks: 100 bytes, 200 bytes, 300 bytes, 400 bytes and 500 bytes..\n" );
	
	void* addr1 = _malloc( 100 );
	void* addr2 = _malloc( 200 );
	void* addr3 = _malloc( 300 );
	void* addr4 = _malloc( 400 );
	void* addr5 = _malloc( 500 );
	
	dbg_heap( "Heap condition after allocation:", heap_start );
	
	assert( addr1 );
	assert( addr2 );
	assert( addr3 );
	assert( addr4 );
	assert( addr5 );
	
	struct block_header* block1 = heap_start;
	struct block_header* block2 = get_header( addr2 );
	struct block_header* block3 = get_header( addr3 );
	struct block_header* block4 = get_header( addr4 );
	struct block_header* block5 = get_header( addr5 );
	
	assert( block1->contents == addr1 );
	assert( !block1->is_free );
	assert_next_and_continious( block1, block2 );
	
	assert( !block2->is_free );
	assert_next_and_continious( block2, block3 );
	
	assert( !block3->is_free );
	assert_next_and_continious( block3, block4 );
	
	assert( !block4->is_free );
	assert_next_and_continious( block4, block5 );
	
	assert( !block5->is_free );
	
	const size_t expected_capacity = block2->capacity.bytes + block3->capacity.bytes + offsetof( struct block_header, contents );
	
	fprintf( stderr, "\nAllocating second and third blocks..\n" );
	
	_free( addr3 );
	
	assert( block3->is_free );
	
	_free( addr2 );
	
	assert( block2->is_free );
	assert( block2->capacity.bytes == expected_capacity ); 
	assert( block2->next == block4 );
	
	dbg_heap( "Heap condition after freeing:", heap_start );
	
	fprintf( stdout, "\nTest passed!\n" );
	
	_free( addr5 );
	_free( addr4 );
	_free( addr1 );
}

void test4( void* heap_start ) {
	fprintf( stdout, "\nTEST: Growing heap\n" );
	
	dbg_heap( "Heap condition before allocation:", heap_start );
	
	fprintf( stderr, "\nAllocating block occupying the heap..\n" );
	
	struct block_header* block1 = heap_start;
	void* addr1 = _malloc( block1->capacity.bytes );
	
	assert( addr1 );
	assert( block1->contents == addr1 );
	assert( !block1->is_free );
	assert( !block1->next );
	
	dbg_heap( "Heap condition after first allocation:", heap_start );
	
	fprintf( stderr, "\nAllocating block 100 bytes and growing heap..\n" );
	
	void* addr2 = _malloc( 100 );
	struct block_header* block2 = get_header( addr2 );
	
	assert_next_and_continious( block1, block2 );
	assert( !block2->is_free );
	assert( block2->capacity.bytes == 100 );
	
	dbg_heap( "Heap condition after second allocation:", heap_start );
	
	fprintf( stdout, "\nTest passed!\n" );
	
	_free( addr2 );
	_free( addr1 );
}

void test5( void* heap_start ) {
	fprintf( stdout, "\nTEST: Growing heap and allocating region does not extending previous\n" );
	
	dbg_heap( "Heap condition before allocation:", heap_start );
	
	fprintf( stderr, "\nAllocating block occupying the heap..\n" );
	
	struct block_header* block1 = heap_start;
	void* addr1 = _malloc( block1->capacity.bytes );
	
	dbg_heap( "Heap condition after first allocation:", heap_start );
	
	assert( block1->contents == addr1 );
	assert( !block1->is_free );
	assert( !block1->next );
	
	fprintf( stderr, "\nMapping region extending previous heap..\n" );
	
	void* next_block_addr = block_after( block1 );
	void* mapped_addr = mmap( next_block_addr, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
	
	assert( mapped_addr == next_block_addr );
	
	fprintf( stderr, "\nAllocating block in another place..\n" );
	
	void* addr2 = _malloc( 200 );
	struct block_header* block2 = get_header( addr2 );
	
	dbg_heap( "Heap condition after second allocation:", heap_start );
	
	assert( next_block_addr != block2 );
	assert( next_block_addr != block2 );
	assert( !block2->is_free );
	
	fprintf( stdout, "\nTest passed!\n" );
	
	_free( addr2 );
	
	_free( addr1 );
}

